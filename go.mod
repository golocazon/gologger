module gitlab.com/golocazon/gologger

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/zap v1.13.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0

)
